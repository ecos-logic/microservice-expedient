package org.ecos.logic.springbootmicroservice.d4ps.expedient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpedientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpedientApplication.class, args);
    }

}
