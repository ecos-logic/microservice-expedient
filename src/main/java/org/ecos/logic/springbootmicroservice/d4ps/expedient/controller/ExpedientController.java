package org.ecos.logic.springbootmicroservice.d4ps.expedient.controller;

import jakarta.validation.Valid;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenAndExpedientRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.ExpedientRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.ExpedientResponse;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.entity.Expedient;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.mapping.ExpedientMapper;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.repository.CitizenRepository;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.repository.ExpedientRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.Cloner.copyFromRequestToEntity;

@RestController
@RequestMapping("/expedient")
public class ExpedientController {
    private final ExpedientMapper mapper;
    private final ExpedientRepository repository;
    private final CitizenRepository citizenRepository;

    public ExpedientController(ExpedientMapper mapper, ExpedientRepository repository, CitizenRepository citizenRepository) {
        this.mapper = mapper;
        this.repository = repository;
        this.citizenRepository = citizenRepository;
    }

    @GetMapping("/")
    public ResponseEntity<List<ExpedientResponse>> listAll(){
        List<ExpedientResponse> expedients = mapper.expedientListToDtoList(repository.findAll());
        return ResponseEntity.ok().body(expedients);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExpedientResponse> listBy(@PathVariable Long id){
        return repository.findById(id).
                map(expedient -> ResponseEntity.ok().body(mapper.expedientToExpedientResponse(expedient))).
                orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/benefitType/{benefit}")
    public ResponseEntity<List<ExpedientResponse>> findByBenefitType(@PathVariable(value = "benefit") long benefit){
        List<Expedient> expedientCollection = repository.findByKindOfBenefit(benefit);

        if(expedientCollection.isEmpty())
            return ResponseEntity.notFound().build();

        List<ExpedientResponse> expedients = mapper.expedientListToDtoList(expedientCollection);

        return ResponseEntity.ok().body(expedients);
    }
    @GetMapping("/taxId/{taxId}")
    public ResponseEntity<List<ExpedientResponse>> findByBenefitType(@PathVariable(value = "taxId") String taxId){
        List<Expedient> expedientCollection = repository.findByTaxId(taxId);

        if(expedientCollection.isEmpty())
            return ResponseEntity.notFound().build();

        List<ExpedientResponse> expedients = mapper.expedientListToDtoList(expedientCollection);

        return ResponseEntity.ok().body(expedients);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBy(@PathVariable Long id){
        return repository.
            findById(id).
            map(expedient ->{
                this.repository.deleteById(id);
                return ResponseEntity.noContent().build();
            }).
            orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody CitizenAndExpedientRequest citizenAndExpedientRequest, BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            StringBuilder explanation = prepareErrorMessage(bindingResult);
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(explanation.toString());
        }

        String taxId = citizenAndExpedientRequest.getTaxId();
        long kindOfBenefit = citizenAndExpedientRequest.getKindOfBenefit();
        Optional<Expedient> optionalExpedient = this.repository.findByTaxIdAndKindOfBenefit(taxId, kindOfBenefit);
        if(optionalExpedient.isPresent()) {
            return ResponseEntity.
                status(HttpStatus.PRECONDITION_FAILED).
                body(
                    format(
                        "There's another expedient (id=%s) with the same kind of benefit (%s) and the same taxId (%s).",
                        optionalExpedient.get().getId(),
                        taxId,
                        kindOfBenefit
                )
            );
    }

        Expedient expedient = this.mapper.citizenAndExpedientRequestToExpedient(citizenAndExpedientRequest);

        CitizenRequest citizenRequest = this.mapper.citizenAndExpedientRequestToCitizenRequest(citizenAndExpedientRequest);
        Long citizenSaved = this.citizenRepository.save(citizenRequest);
        if(citizenSaved <= 0)
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).build();

        expedient.setCreateAt(new Date());
        Expedient savedExpedient = this.repository.save(expedient);

        ExpedientResponse result = mapper.expedientToExpedientResponse(savedExpedient);

        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @PutMapping("{id}")
    public ResponseEntity<ExpedientResponse> update(@PathVariable Long id,@RequestBody ExpedientRequest expedientRequest){

        Optional<Expedient> optionalExpedient = repository.findById(id);

        if(optionalExpedient.isEmpty())
            return ResponseEntity.notFound().build();


        Expedient savedExpedient = copyFromRequestToEntity(expedientRequest,optionalExpedient.get());


        this.repository.save(savedExpedient);

        ExpedientResponse result = mapper.expedientToExpedientResponse(savedExpedient);

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    private static StringBuilder prepareErrorMessage(BindingResult bindingResult) {
        StringBuilder explanation = new StringBuilder();

        for (FieldError error: bindingResult.getFieldErrors()) {
            explanation.append(format("%s\t %s", error.getField(), error.getDefaultMessage())).append("\n");
        }
        return explanation;
    }
}
