package org.ecos.logic.springbootmicroservice.d4ps.expedient.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@SuppressWarnings("unused")
public class CitizenAndExpedientRequest {
    @NotEmpty
    @Size(min = 1,max = 20)
    private String name;
    @NotEmpty
    @Size(min = 1,max = 30)
    private String surname;
    @NotEmpty
    @Size(min = 1,max = 30)
    private String secondSurname;
    @NotEmpty
    @Size(min = 1,max = 40)
    private String email;

    @Min(1)
    @Max(3)
    private long kindOfBenefit;
    @NotEmpty
    @Size(min = 1,max = 80)
    private String notes;
    @NotEmpty
    @Size(min = 1,max = 9)
    private String taxId;

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getKindOfBenefit() {
        return kindOfBenefit;
    }

    public void setKindOfBenefit(long kindOfBenefit) {
        this.kindOfBenefit = kindOfBenefit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
}
