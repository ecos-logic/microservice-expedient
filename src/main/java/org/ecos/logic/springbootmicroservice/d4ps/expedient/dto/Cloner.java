package org.ecos.logic.springbootmicroservice.d4ps.expedient.dto;

import org.ecos.logic.springbootmicroservice.d4ps.expedient.entity.Expedient;

public class Cloner {
    private Cloner() {}

    public static Expedient copyFromRequestToEntity(ExpedientRequest expedientRequest, Expedient expedient) {
        expedient.setKindOfBenefit(expedientRequest.getKindOfBenefit());
        expedient.setNotes(expedientRequest.getNotes());
        expedient.setCreateAt(expedientRequest.getDateOfCreation());
        expedient.setTaxId(expedientRequest.getTaxId());

        return expedient;
    }
}
