package org.ecos.logic.springbootmicroservice.d4ps.expedient.dto;

import java.util.Date;

@SuppressWarnings("unused")
public class ExpedientRequest {
    private long kindOfBenefit;
    private String notes;
    private String taxId;
    private Date dateOfCreation;

    public ExpedientRequest(long kindOfBenefit, String notes, Date dateOfCreation) {
        this.kindOfBenefit = kindOfBenefit;
        this.notes = notes;
        this.dateOfCreation = dateOfCreation;
    }

    public long getKindOfBenefit() {
        return kindOfBenefit;
    }

    public void setKindOfBenefit(long kindOfBenefit) {
        this.kindOfBenefit = kindOfBenefit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

}
