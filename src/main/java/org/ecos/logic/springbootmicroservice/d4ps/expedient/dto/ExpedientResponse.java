package org.ecos.logic.springbootmicroservice.d4ps.expedient.dto;

import java.util.Date;

@SuppressWarnings("unused")
public class ExpedientResponse {
    private long id;
    private long kindOfBenefit;
    private String notes;
    private String taxId;
    private Date dateOfCreation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getKindOfBenefit() {
        return kindOfBenefit;
    }

    public void setKindOfBenefit(long kindOfBenefit) {
        this.kindOfBenefit = kindOfBenefit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
