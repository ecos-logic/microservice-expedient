package org.ecos.logic.springbootmicroservice.d4ps.expedient.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Date;

@SuppressWarnings("unused")
@Entity
public class Expedient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long kindOfBenefit;

    private String notes;

    private String taxId;

    private Date createAt;

    public long getId() {
        return id;
    }

    public long getKindOfBenefit() {
        return kindOfBenefit;
    }

    public String getNotes() {
        return notes;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setKindOfBenefit(long kindOfBenefit) {
        this.kindOfBenefit = kindOfBenefit;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}
