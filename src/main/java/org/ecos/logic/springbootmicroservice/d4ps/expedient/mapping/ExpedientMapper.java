package org.ecos.logic.springbootmicroservice.d4ps.expedient.mapping;

import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenAndExpedientRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.ExpedientRequest;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.ExpedientResponse;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.entity.Expedient;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExpedientMapper {
    @Mapping(source = "createAt", target = "dateOfCreation")
    ExpedientResponse expedientToExpedientResponse(Expedient expedient);

    List<ExpedientResponse> expedientListToDtoList(List<Expedient> expedient);

    @SuppressWarnings("unused")
    @Mapping(source = "dateOfCreation", target = "createAt")
    Expedient expedientRequestToExpedient(ExpedientRequest expedientRequest);

    @Mapping(target = "createAt",ignore = true)
    Expedient citizenAndExpedientRequestToExpedient(CitizenAndExpedientRequest expedientRequest);

    CitizenRequest citizenAndExpedientRequestToCitizenRequest(CitizenAndExpedientRequest citizenAndExpedientRequest);
}
