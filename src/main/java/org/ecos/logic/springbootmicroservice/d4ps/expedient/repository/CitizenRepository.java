package org.ecos.logic.springbootmicroservice.d4ps.expedient.repository;

import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenRequest;

public interface CitizenRepository {
    Long save(CitizenRequest citizenRequest);
}
