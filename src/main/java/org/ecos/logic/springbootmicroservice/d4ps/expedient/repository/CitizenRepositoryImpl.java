package org.ecos.logic.springbootmicroservice.d4ps.expedient.repository;

import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.ecos.logic.springbootmicroservice.d4ps.expedient.dto.CitizenRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

@Component
public class CitizenRepositoryImpl implements CitizenRepository {
    private final WebClient.Builder webClientBuilder;

    public CitizenRepositoryImpl(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    HttpClient client = HttpClient.create().
        option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000).
        option(ChannelOption.SO_KEEPALIVE, true).
        option(EpollChannelOption.TCP_KEEPIDLE, 300).
        option(EpollChannelOption.TCP_KEEPINTVL, 60).
        responseTimeout(Duration.ofSeconds(1))
        .doOnConnected(connection -> {
            connection.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS));
            connection.addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS));
        });

    @Override
    public Long save(CitizenRequest citizenRequest) {
        WebClient build = webClientBuilder.clientConnector(new ReactorClientHttpConnector(client))
                .baseUrl("http://localhost:8092/citizen")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", "http://localhost:8092/citizen"))
                .build();
        return build.method(HttpMethod.POST).uri("/createCitizen")
                .body(Mono.just(citizenRequest),CitizenRequest.class).retrieve().bodyToMono(Long.class).block();

    }
}
