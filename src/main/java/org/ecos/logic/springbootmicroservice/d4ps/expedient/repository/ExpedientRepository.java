package org.ecos.logic.springbootmicroservice.d4ps.expedient.repository;

import org.ecos.logic.springbootmicroservice.d4ps.expedient.entity.Expedient;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ExpedientRepository extends JpaRepository<Expedient, Long> {

    @Query("SELECT expedient FROM Expedient expedient WHERE expedient.kindOfBenefit = ?1")
    List<Expedient> findByKindOfBenefit(Long benefit);

    @Query("SELECT expedient FROM Expedient expedient WHERE expedient.taxId = ?1")
    List<Expedient> findByTaxId(String taxId);

    @Query("SELECT expedient FROM Expedient expedient WHERE expedient.taxId = ?1 and expedient.kindOfBenefit = ?2")
    Optional<Expedient> findByTaxIdAndKindOfBenefit(String taxId, long kindOfBenefit);
}
